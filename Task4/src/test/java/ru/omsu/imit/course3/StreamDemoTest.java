package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StreamDemoTest {
    @Test
    public void testTransform() {
        List<Integer> list = new ArrayList<>();
        list.add(23);
        list.add(-12);
        list.add(64);
        list.add(-84);
        list.add(3);
        list.add(143);
        list.add(-123);
        StreamDemo.transform(list.stream().mapToInt(a->a), a->a).forEach(System.out::println);
        System.out.println();
        StreamDemo.transformPar(list.stream().mapToInt(a->a), a->a).forEach(System.out::println);

    }
    @Test
    public void testGetNamesOlderThanThirty () {
        List<Person> list = new ArrayList<>();
        list.add(new Person("A", 33));
        list.add(new Person("B", 34));
        list.add(new Person("X", 28));
        list.add(new Person("B", 234));
        List<String> res = new ArrayList<>();
        res.add("A");
        res.add("B");
        Assert.assertEquals(res, StreamDemo.getNamesOlderThirty(list));
    }
    @Test
    public void testGetNamesOlderThanThirtyByCount () {
        List<Person> list = new ArrayList<>();
        list.add(new Person("A", 33));
        list.add(new Person("B", 34));
        list.add(new Person("B", 34));
        list.add(new Person("S", 29));
        list.add(new Person("X", 3));
        list.add(new Person("C", 154));
        list.add(new Person("C", 88));
        list.add(new Person("C", 54));
        List<String> res = new ArrayList<>();
        res.add("A");
        res.add("B");
        res.add("C");
        Assert.assertEquals(res, StreamDemo.getNamesOlderThirtySortedByCount(list));
    }
    @Test
    public void testSumProduct() {
        List<Integer> list = new ArrayList<>();
        list.clear();
        list.add(2);
        list.add(4);
        list.add(5);
        list.add(1);
        list.add(3);
        Assert.assertEquals(15, StreamDemo.sum(list));
        Assert.assertEquals(120, StreamDemo.product(list));
    }
}
