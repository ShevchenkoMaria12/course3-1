package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LambdaDemoTest {
    @Test
    public void splitAndCountTest() {
        String str = "asd fgh jkl";
        Integer result = 3;
        Assert.assertEquals(result, LambdaDemo.count.apply(LambdaDemo.split.apply(str)));
    }
    @Test
    public void splitAndCountWithoutTypeTest() {
        String str = "asd fgh jkl";
        Integer result = 3;
        Assert.assertEquals(result, LambdaDemo.countWhitoutType.apply(LambdaDemo.splitWhitoutType.apply(str)));
    }
    @Test
    public void countMethodReferenceTest() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = 3;
        Assert.assertEquals(LambdaDemo.countMethodReference.apply(list), result);
    }
    @Test
    public void andThenComposeTest() {
        String str1 = "asd fgh";
        String str2 = "asd fgh jkl";
        Integer result1 = 2;
        Integer result2 = 3;
        Assert.assertEquals(LambdaDemo.splitAndThenCount.apply(str1), result1);
        Assert.assertEquals(LambdaDemo.countComposeSplit.apply(str2), result2);
    }
    @Test
    public void createTest() {
        String name1 = "Ivan";
        Person person1 = new Person(name1);
        String name2 = "Peter";
        Person person2 = new Person(name2);
        Person actual = LambdaDemo.create.apply(name1);
        Assert.assertNotNull(actual);
        Assert.assertEquals(name1, actual.getName());
        Assert.assertEquals(person1, actual);
        actual = LambdaDemo.createMethodReference.apply(name2);
        Assert.assertNotNull(actual);
        Assert.assertEquals(name2, actual.getName());
        Assert.assertEquals(person2, actual);
    }
    @Test
    public void maxTest() {
        Integer result = 5;
        Assert.assertEquals(result, LambdaDemo.max.apply(2, 5));
        result = -2;
        Assert.assertEquals(result, LambdaDemo.max.apply(-2, -5));
    }
    @Test
    public void dateTest() {
        Date expected = new Date();
        Date actual = LambdaDemo.getCurrentDate.get();
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected.getDate(), actual.getDate());
    }
    @Test
    public void isEvenTest() {
        Assert.assertFalse(LambdaDemo.isEven.test(11));
        Assert.assertTrue(LambdaDemo.isEven.test(12));
    }
    @Test
    public void isEqualTest() {
        Assert.assertTrue(LambdaDemo.isEqual.test(11, 11));
        Assert.assertFalse(LambdaDemo.isEqual.test(13, 24));
    }
}
