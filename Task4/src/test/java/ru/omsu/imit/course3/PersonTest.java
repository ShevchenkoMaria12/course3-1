package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class PersonTest {
    @Test
    public void getMothersMotherFatherTest() {
        Person1 father = new Person1("f", null, null);
        Person1 person = new Person1("m", null, new Person1("mm",
                null, new Person1("mmm", father, null)));
        Assert.assertEquals(father, person.getMothersMotherFather());
    }
    @Test
    public void getMothersMotherFatherNullTest() {
        Person1 person = new Person1("f", null, new Person1("m",
                null, new Person1("mm", null, null)));
        Assert.assertNull(person.getMothersMotherFather());
    }
    @Test
    public void getMothersMotherFatherWithOptionalTest() {
        Person2 father = new Person2("f", null, null);
        Person2 person = new Person2("m", null, new Person2("mm",
                null, new Person2("mmm", father, null)));
        Assert.assertEquals(Optional.ofNullable(father), person.getMothersMotherFather());
    }

    @Test
    public void getMothersMotherFatherNullWithOptionalTest() {
        Person2 test = new Person2("f", null, new Person2("m",
                null, new Person2("mm", null, null)));
        Assert.assertEquals(Optional.empty(),test.getMothersMotherFather());
    }
}
