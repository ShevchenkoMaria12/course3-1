package ru.omsu.imit.course3;

import java.util.Objects;
import java.util.Optional;

public class Person2 {
    private String name;
    private Optional<Person2> father;
    private Optional<Person2> mother;
    public Person2(String name, Person2 father, Person2 mother) {
        this.setName(name);
        this.setFather(father);
        this.setMother(mother);
    }
    public Optional<Person2> getMothersMotherFather () {
        return mother.flatMap(p -> p.getMother()).flatMap(d -> d.getFather());
    }
    public String getName() {
        return name;
    }
    public Optional<Person2> getFather() {
        return father;
    }
    public Optional<Person2> getMother() {
        return mother;
    }
    public void setName(String name) {
        if(name != null || name.length() != 0)
            this.name = name;
    }
    public void setFather(Person2 father) {
        this.father = Optional.ofNullable(father);
    }
    public void setMother(Person2 mother) {
        this.mother = Optional.ofNullable(mother);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person2 person2 = (Person2) o;
        return Objects.equals(name, person2.name) &&
                Objects.equals(father, person2.father) &&
                Objects.equals(mother, person2.mother);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, father, mother);
    }
}
