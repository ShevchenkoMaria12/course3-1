package ru.omsu.imit.course3;

@FunctionalInterface
public interface MyFunction<T, K> {
    K apply(T arg);
}
