package ru.omsu.imit.course3;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamDemo {
    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    public static IntStream transformPar(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().map(op);
    }

    public static List<String> getNamesOlderThirty(List<Person> list) {
        return list.stream().filter(person -> person.getAge() > 30).map(Person::getName).distinct()
                .sorted(Comparator.comparingInt(String::length)).collect(Collectors.toList());
    }

    public static List<String> getNamesOlderThirtySortedByCount(List<Person> list) {
        return list.stream().filter(person -> person.getAge() > 30).collect(Collectors.groupingBy(
                Person::getName, Collectors.counting())).entrySet().stream().sorted(Comparator
                .comparing(Map.Entry::getValue)).map(Map.Entry::getKey).collect(Collectors.toList());
    }

    public static int sum(List<Integer> list) {
        return list.stream().reduce((res, s) -> res += s).orElse(0);
    }

    public static int product(List<Integer> list) {
        return list.stream().reduce((res, s) -> res *= s).orElse(0);
    }
}
