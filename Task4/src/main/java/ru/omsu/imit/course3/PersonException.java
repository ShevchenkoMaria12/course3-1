package ru.omsu.imit.course3;

public class PersonException extends Exception{
    public PersonException() {
        super();
    }
    public PersonException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
}
