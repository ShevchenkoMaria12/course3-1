package ru.omsu.imit.course3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.*;

public class LambdaDemo {

    public static final Function<String, List<String>> split = (String s) -> Arrays.asList(s.split(" "));

    public static final Function<List<?>, Integer> count = (List<?> list) -> list.size();

    public static final MyFunction<String, List<String>> splitWhitoutType = s -> Arrays.asList(s.split(" "));

    public static final MyFunction<List<?>, Integer> countWhitoutType = list -> list.size();

    public static final Function<List<?>, Integer> countMethodReference = List::size;

    public static final Function<String, Integer> splitAndThenCount = split.andThen(count);

    public static final Function<String, Integer> countComposeSplit = count.compose(split);

    public static final Function<String, Person> create = s -> new Person(s);

    public static final Function<String, Person> createMethodReference = Person::new;

    public static final BiFunction<Integer, Integer, Integer> max = Math::max;

    public static final Supplier<Date> getCurrentDate = Date::new;

    public static final Predicate<Integer> isEven = a -> a % 2 == 0;

    public static final BiPredicate<Integer, Integer> isEqual = Integer::equals;
}
