package ru.omsu.imit.course3;

import java.util.Objects;

public class Person {
    private String name;
    private int age;
    public Person(String name) {
        this.setName(name);
    }
    public Person(String name, int age) {
        this.setName(name);
        this.setAge(age);
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public void setName(String name) {
        if(name != null || name.length() != 0)
        this.name = name;
    }
    public void setAge(int age) {
        if(age > 0)
        this.age = age;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

