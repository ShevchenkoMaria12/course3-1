package ru.omsu.imit.course3;

import java.util.Objects;

public class Person1 {
    private String name;
    private Person1 father;
    private Person1 mother;
    public Person1(String name, Person1 father, Person1 mother) {
        this.setName(name);
        this.setFather(father);
        this.setMother(mother);
    }
    public Person1 getMothersMotherFather () {
        if (this.mother == null || this.mother.mother == null) {
            return null;
        }
        return this.mother.mother.father;
    }
    public String getName() {
        return name;
    }
    public Person1 getFather() {
        return father;
    }
    public Person1 getMother() {
        return mother;
    }
    public void setName(String name) {
        if(name != null || name.length() != 0)
            this.name = name;
    }
    public void setFather(Person1 father) {
        if(father != null)
            this.father = father;
    }
    public void setMother(Person1 mother) {
        if(mother != null)
            this.mother = mother;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person1 person1 = (Person1) o;
        return Objects.equals(name, person1.name) &&
                Objects.equals(father, person1.father) &&
                Objects.equals(mother, person1.mother);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, father, mother);
    }
}
