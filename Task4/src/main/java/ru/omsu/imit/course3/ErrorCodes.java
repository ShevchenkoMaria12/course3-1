package ru.omsu.imit.course3;

public enum ErrorCodes {
    WRONG_NAME("Wrong name"),
    WRONG_AGE("Wrong age");
    private String message;
    private ErrorCodes(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
