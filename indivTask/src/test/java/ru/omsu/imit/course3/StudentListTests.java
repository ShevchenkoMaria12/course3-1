package ru.omsu.imit.course3;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StudentListTests {
    @Test
    public void testAdd() throws PersonException {
        StudentList studentList = new StudentList();
        Student s1 = new Student("I", "I", "I", Gender.MALE, true, false,
                false);
        Student s2 = new Student("A", "A", "A", Gender.FEMALE, true, true,
                false);
        Student s3 = new Student("V", "V", "V", Gender.MALE, false, false,
                true);
        studentList.addStudent(s1);
        studentList.addStudent(s2);
        studentList.addStudent(s3);
        List<Student> list = new ArrayList<Student>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        assertEquals(studentList.getStudentList(),list);
    }
    @Test
    public void testRemove() throws PersonException {
        StudentList studentList = new StudentList();
        Student s1 = new Student("I", "I", "I", Gender.MALE, true, false,
                false);
        Student s2 = new Student("A", "A", "A", Gender.FEMALE, true, true,
                false);
        Student s3 = new Student("V", "V", "V", Gender.MALE, false, false,
                true);
        studentList.addStudent(s1);
        studentList.addStudent(s2);
        studentList.addStudent(s3);
        studentList.removeStudent(0);
        List<Student> list = new ArrayList<Student>();
        list.add(s2);
        list.add(s3);
        assertEquals(studentList.getStudentList(),list);
    }
    @Test
    public void testSet() throws PersonException {
        StudentList studentList = new StudentList();
        Student s1 = new Student("I", "I", "I", Gender.MALE, true, false,
                false);
        Student s2 = new Student("A", "A", "A", Gender.FEMALE, true, true,
                false);
        Student s3 = new Student("V", "V", "Ivanov", Gender.MALE, false, false,
                true);
        Student s4 = new Student("B", "B", "B", Gender.MALE, true, false,
                true);
        studentList.addStudent(s1);
        studentList.addStudent(s2);
        studentList.addStudent(s3);
        studentList.setStudent(0, s4);
        List<Student> list = new ArrayList<Student>();
        list.add(s4);
        list.add(s2);
        list.add(s3);
        assertEquals(studentList.getStudentList(),list);
    }
    @Test
    public void testGet() throws PersonException {
        StudentList studentList = new StudentList();
        Student s1 = new Student("I", "I", "I", Gender.MALE, true, false,
                false);
        Student s2 = new Student("A", "A", "A", Gender.FEMALE, true, true,
                false);
        Student s3 = new Student("V", "V", "V", Gender.MALE, false, false,
                true);
        studentList.addStudent(s1);
        studentList.addStudent(s2);
        studentList.addStudent(s3);
        assertEquals(studentList.getStudent(0), s1);
    }
    @Test
    public void testGetSize() throws PersonException {
        StudentList studentList = new StudentList();
        Student s1 = new Student("I", "I", "I", Gender.MALE, true, false,
                false);
        Student s2 = new Student("A", "A", "A", Gender.FEMALE, true, true,
                false);
        Student s3 = new Student("V", "V", "V", Gender.MALE, false, false,
                true);
        studentList.addStudent(s1);
        studentList.addStudent(s2);
        studentList.addStudent(s3);
        assertEquals(studentList.getSize(), 3);
    }
}
