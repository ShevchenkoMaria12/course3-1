package ru.omsu.imit.course3;

public enum ErrorCodes {
    WRONG_FIRSTNAME("Wrong first name %s"),
    WRONG_MIDNAME("Wrong mid name %s"),
    WRONG_LASTNAME("Wrong last name %s");
    private String message;
    private ErrorCodes(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
