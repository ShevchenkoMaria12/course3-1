package ru.omsu.imit.course3;

import java.util.Objects;

public abstract class Person {
    private String firstName, midName, lastName;
    private Gender gender;
    Person(String firstName, String midName, String lastName, Gender gender) throws PersonException {
        this.setFirstName(firstName);
        this.setMidName(midName);
        this.setLastName(lastName);
        this.setGender(gender);
    }
    public String getFirstName() {
        return firstName;
    }
    public String getMidName() {
        return midName;
    }
    public String getLastName() {
        return lastName;
    }
    public Gender getGender() {
        return gender;
    }
    public void setFirstName(String firstName) throws PersonException {
        checkFirstName(firstName);
        this.firstName = firstName;
    }
    public void setMidName(String midName) throws PersonException {
        checkMidName(midName);
        this.midName = midName;
    }
    public void setLastName(String lastName) throws PersonException {
        checkLastName(lastName);
        this.lastName = lastName;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    private static void checkFirstName(String firstName) throws PersonException {
        if(firstName == null || firstName.length() == 0)
            throw new PersonException(ErrorCodes.WRONG_FIRSTNAME, firstName);
    }
    private static void checkMidName(String midName) throws PersonException {
        if(midName == null || midName.length() == 0 )
            throw new PersonException(ErrorCodes.WRONG_MIDNAME, midName);
    }
    private static void checkLastName(String lastName) throws PersonException {
        if(lastName == null || lastName.length() == 0 )
            throw new PersonException(ErrorCodes.WRONG_LASTNAME, lastName);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(midName, person.midName) &&
                Objects.equals(lastName, person.lastName) &&
                gender == person.gender;
    }
    @Override
    public int hashCode() {
        return Objects.hash(firstName, midName, lastName, gender);
    }
}
