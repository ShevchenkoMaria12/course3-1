package ru.omsu.imit.course3;

import java.util.Objects;

public class Student extends Person {
    private Boolean location, is3, isExcellent;
    public Student(String firstName, String midName, String lastName,
                   Gender gender, Boolean location, Boolean is3, Boolean isExcellent) throws PersonException {
        super(firstName, midName, lastName, gender);
        this.setLocation(location);
        this.setIs3(is3);
        this.setExcellent(isExcellent);
    }
    public Boolean getLocation() { return location; }
    public Boolean getIs3() { return is3; }
    public Boolean getExcellent() { return isExcellent; }
    public void setLocation(Boolean location) { this.location = location; }
    public void setIs3(Boolean is3) {
        this.is3 = is3;
    }
    public void setExcellent(Boolean excellent) { isExcellent = excellent; }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Objects.equals(location, student.location) &&
                Objects.equals(is3, student.is3) &&
                Objects.equals(isExcellent, student.isExcellent);
    }
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), location, is3, isExcellent);
    }
}
