package ru.omsu.imit.course3;

class PersonException extends Exception {
    public PersonException() {
        super();
    }
    public PersonException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
    public PersonException(ErrorCodes errorCode, String param) {
        super(String.format(errorCode.getMessage(), param));
    }
}