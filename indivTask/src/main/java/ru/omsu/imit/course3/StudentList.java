package ru.omsu.imit.course3;

import java.util.ArrayList;
import java.util.List;

public class StudentList {
    private List<Student> studentList;
    public StudentList(){
        studentList = new ArrayList<>();
    }
    public void addStudent(Student student){
        studentList.add(student);
    }
    public void removeStudent(int index){
        studentList.remove(index);
    }
    public void setStudent(int index, Student student) {
        studentList.set(index, student);
    }
    public Student getStudent(int index) {
        return studentList.get(index);
    }
    public int getSize() {
        return studentList.size();
    }
    public List<Student> getStudentList() {
        return studentList;
    }
}
