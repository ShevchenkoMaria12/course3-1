package ru.omsu.imit.course3;

public class TraineeException extends Exception {
    public TraineeException() {
        super();
    }
    public TraineeException(TraineeErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
    public TraineeException(TraineeErrorCodes errorCode, String param) {
        super(String.format(errorCode.getMessage(), param));
    }
}
