package ru.omsu.imit.course3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class TraineeTests {
    @Test(expected = TraineeException.class)
    public void testTrainee() throws TraineeException {
        Trainee trainee1 = new Trainee("Ivan", "Ivanov", 4);
        Trainee trainee2 = new Trainee("Ivan", "Ivanov", 10);
    }
    @Test(expected = TraineeException.class)
    public void testSet1() throws TraineeException {
        Trainee trainee1 = new Trainee(null, null, 10);
        Trainee trainee2 = new Trainee("Ivan", "Ivanov", 5);
        trainee1.setFirstName("Ivan");
        trainee1.setLastName("Ivanov");
        trainee1.setGrade(5);
        assertEquals(trainee1, trainee2);
    }
    @Test
    public void testSet2() throws TraineeException {
        Trainee trainee1 = new Trainee("Peter", "Petrov", 1);
        Trainee trainee2 = new Trainee("Ivan", "Ivanov", 5);
        trainee1.setFirstName("Ivan");
        trainee1.setLastName("Ivanov");
        trainee1.setGrade(5);
        assertEquals(trainee1, trainee2);
    }
    @Test
    public void inputOutputByNewLineTest() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("text1.txt");
        TraineeDemo.outputByNewLine(trainee,file);
        assertEquals(TraineeDemo.inputByNewLine(file),trainee);
    }
    @Test
    public void inputOutputBySpaceTest() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("text2.txt");
        TraineeDemo.outputBySpace(trainee,file);
        assertEquals(TraineeDemo.inputBySpace(file),trainee);
    }
    @Test
    public void fileSerializationTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("text3.txt");
        TraineeDemo.fileSerialization(trainee,file);
        assertEquals(TraineeDemo.fileDeserialization(file),trainee);
    }
    @Test
    public void fileDeserializationTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        byte[] bytes = TraineeDemo.byteArraySerialization(trainee);
        assertEquals(TraineeDemo.byteArrayDeserialization(bytes),trainee);
    }
    @Test
    public void inputOutputJsonTest() throws TraineeException{
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        assertEquals(TraineeDemo.inputJson(TraineeDemo.outputJson(trainee)),trainee);
    }
    @Test
    public void inputOutputFileJsonTest() throws TraineeException, IOException{
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("text4.txt");
        TraineeDemo.outputFileJson(trainee,file);
        assertEquals(TraineeDemo.inputFileJson(file),trainee);
    }
}
