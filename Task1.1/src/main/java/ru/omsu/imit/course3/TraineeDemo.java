package ru.omsu.imit.course3;

import com.google.gson.Gson;

import java.io.*;

public class TraineeDemo {
    public static void outputByNewLine(Trainee trainee, File file) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintStream printStream = new PrintStream(file, "UTF-8")) {
            printStream.println(trainee.getFirstName());
            printStream.println(trainee.getLastName());
            printStream.println(trainee.getGrade());
        }
    }
    public static Trainee inputByNewLine(File file) throws IOException, TraineeException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            return new Trainee(br.readLine(), br.readLine(), Integer.parseInt(br.readLine()));
        }
    }
    public static void outputBySpace(Trainee trainee, File file) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintStream printStream = new PrintStream(file, "UTF-8")) {
            printStream.print(trainee.getFirstName() + " " + trainee.getLastName() + " " + trainee.getGrade());
        }
    }

    public static Trainee inputBySpace(File file) throws IOException, TraineeException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String[] s = reader.readLine().split(" ");
            return new Trainee(s[0], s[1], Integer.parseInt(s[2]));
        }
    }
    public static void fileSerialization(Trainee trainee, File file) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee fileDeserialization(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (Trainee) ois.readObject();
        }
    }
    public static byte[] byteArraySerialization(Trainee trainee) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(trainee);
        }
        return baos.toByteArray();
    }
    public static Trainee byteArrayDeserialization(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
            return (Trainee)ois.readObject();
        }
    }
    public static String outputJson(Trainee trainee)  {
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    public static Trainee inputJson(String s) {
        Gson gson = new Gson();
        return gson.fromJson(s, Trainee.class);
    }
    public static void outputFileJson(Trainee trainee, File file) throws IOException {
        Gson gson = new Gson();
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            gson.toJson(trainee, bw);
        }
    }

    public static Trainee inputFileJson(File file) throws IOException {
        Gson gson = new Gson();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            return gson.fromJson(br, Trainee.class);
        }
    }
}
