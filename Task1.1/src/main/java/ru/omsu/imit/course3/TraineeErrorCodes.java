package ru.omsu.imit.course3;

public enum TraineeErrorCodes {
    WRONG_FIRSTNAME("Wrong first name %s"),
    WRONG_LASTNAME("Wrong last name %s"),
    WRONG_GRADE("Wrong grade name %s");
    private String message;
    private TraineeErrorCodes(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
