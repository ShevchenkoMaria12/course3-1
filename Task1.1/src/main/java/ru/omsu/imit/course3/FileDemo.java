package ru.omsu.imit.course3;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class FileDemo {
    public static void main(String[] args) {
        File dir = new File("DIR");
        File dir1 = new File(dir, "dir1");
        File dir2 = new File("testdir/direct");
        dir.mkdir();
        dir1.mkdir();
        System.out.println(dir2.getAbsolutePath());
        System.out.println("dir2.exists: "+dir2.exists());
        dir2.mkdirs();
        System.out.println("dir2.exists: "+dir2.exists());
        System.out.println("dir2.isFile: "+dir2.isFile());
        System.out.println("dir2.isDirectory: "+dir2.isDirectory());
        File file1 = new File(dir, "1.txt");
        File file2 = new File(dir, "2.doc");
        File file3 = new File(dir, "3.txt");
        try {
            file1.createNewFile();
            file2.createNewFile();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        try (DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(
                new FileOutputStream(file1)))) {
            outputStream.writeChars("abc");
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        file1.renameTo(file3);
        file2.delete();
        new File("testdir/direct").delete();
        new File("testdir").delete();

        System.out.println("AbsolutePath: "+file1.getAbsolutePath());
        List<File> files = new ArrayList<>(Arrays.asList(Objects.requireNonNull(new File("dir").
                listFiles())));
        for(File file :files){
            System.out.println(file.getPath());
        }
        System.out.println();
        List<File> filesWithFilter = new ArrayList<>(Arrays.asList(Objects.requireNonNull(
                new File("dir").listFiles(pathname -> pathname.getPath().endsWith(".txt")))));
        for(File file :filesWithFilter){
            System.out.println(file.getPath());
        }
    }
}
