package ru.omsu.imit.course3;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RectangleTests {
    @Test
    public void testBinaryOutputInput() throws IOException {
        Point point1 = new Point(0, 3);
        Point point2 = new Point(4, 0);
        RectangleDemo.binaryOutput(point1, point2, new File("1.dat"));
        assertEquals(RectangleDemo.binaryInput(new File("1.dat")), new Rectangle(point1, point2));
    }
    @Test
    public void rectanglesInverse() throws IOException {
        List<Rectangle> rectangles = new ArrayList<>(Arrays.asList(
                new Rectangle(new Point(0, 3), new Point(4, 0)),
                new Rectangle(new Point(2, 2), new Point(5, 0)),
                new Rectangle(new Point(1, 4), new Point(3, -1)),
                new Rectangle(new Point(-2, 0), new Point(2, -5)),
                new Rectangle(new Point(0, 0), new Point(6, -3))
        ));
        RectangleDemo.listBinaryOutput(rectangles,new File("test.txt"));
        List<Rectangle> rectangles1 = RectangleDemo.listInverseInput(new File("test.txt"));
        List<Rectangle> rectangles2 = new ArrayList<>(Arrays.asList(
                new Rectangle(new Point(0, 0), new Point(6, -3)),
                new Rectangle(new Point(-2, 0), new Point(2, -5)),
                new Rectangle(new Point(1, 4), new Point(3, -1)),
                new Rectangle(new Point(2, 2), new Point(5, 0)),
                new Rectangle(new Point(0, 3), new Point(4, 0))
        ));
        RectangleDemo.printStreamOutput(rectangles1, new File("ps.txt"));
        assertEquals(rectangles1, rectangles2);
    }
}
