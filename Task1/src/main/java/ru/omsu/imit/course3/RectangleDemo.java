package ru.omsu.imit.course3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RectangleDemo {
    public static void binaryOutput(Point leftTopPoint, Point rightBottomPoint, File file) throws IOException {
        Rectangle rectangle = new Rectangle(leftTopPoint, rightBottomPoint);
        try(DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeDouble(rectangle.getLeftTopPoint().getX());
            dos.writeDouble(rectangle.getLeftTopPoint().getY());
            dos.writeDouble(rectangle.getRightBottomPoint().getX());
            dos.writeDouble(rectangle.getRightBottomPoint().getY());
        }
    }
    public static Rectangle binaryInput(File file) throws IOException {
        try(DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            Point point1 = new Point(dis.readDouble(), dis.readDouble());
            Point point2 = new Point(dis.readDouble(), dis.readDouble());
            Rectangle rectangle = new Rectangle(point1, point2);
            return rectangle;
        }
    }
    public static void listBinaryOutput(List<Rectangle> rectangles, File file) throws IOException {
        try(DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            for(Rectangle r : rectangles) {
                dos.writeDouble(r.getLeftTopPoint().getX());
                dos.writeDouble(r.getLeftTopPoint().getY());
                dos.writeDouble(r.getRightBottomPoint().getX());
                dos.writeDouble(r.getRightBottomPoint().getY());
            }
        }
    }
    public static List<Rectangle> listInverseInput(File file) throws IOException {
        List<Rectangle> rectangles1 = new ArrayList<>();
        try(RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            long fileLength = raf.length();
            while (fileLength > 0) {
                fileLength -= 32;
                raf.seek(fileLength);
                rectangles1.add(new Rectangle(new Point(raf.readDouble(), raf.readDouble()), new Point(raf.readDouble(),
                                raf.readDouble())));
            }
        }
        return rectangles1;
    }
    public static void printStreamOutput(List<Rectangle> rectangles, File file) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintStream ps = new PrintStream(file, "UTF-8")) {
            for(Rectangle r : rectangles) {
                ps.print(r.getLeftTopPoint().getX());
                ps.print(r.getLeftTopPoint().getY());
                ps.print(r.getRightBottomPoint().getX());
                ps.print(r.getRightBottomPoint().getY());
                ps.println();
            }
        }
    }

}
