package ru.omsu.imit.course3;

import java.util.Objects;

public class Rectangle {
    private Point leftTopPoint;
    private Point rightBottomPoint;
    public Rectangle(Point leftTopPoint, Point rightBottomPoint) throws NullPointerException {
        this.setLeftTopPoint(leftTopPoint);
        this.setRightBottomPoint(rightBottomPoint);
    }
    public Point getLeftTopPoint() {
        return leftTopPoint;
    }
    public Point getRightBottomPoint() { return rightBottomPoint; }
    public void setLeftTopPoint(Point leftTopPoint) throws NullPointerException {
        checkLeftTopPoint(leftTopPoint);
        this.leftTopPoint = leftTopPoint;
    }
    public void setRightBottomPoint(Point rightBottomPoint) throws NullPointerException {
        checkRightBottomPoint(rightBottomPoint);
        this.rightBottomPoint = rightBottomPoint;
    }
    private static void checkLeftTopPoint(Point leftTopPoint) throws NullPointerException {
        if(leftTopPoint == null)
            throw new NullPointerException();
    }
    private static void checkRightBottomPoint(Point rightBottomPoint) throws NullPointerException {
        if(rightBottomPoint == null)
            throw new NullPointerException();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Objects.equals(leftTopPoint, rectangle.leftTopPoint) &&
                Objects.equals(rightBottomPoint, rectangle.rightBottomPoint);
    }
    @Override
    public int hashCode() {
        return Objects.hash(leftTopPoint, rightBottomPoint);
    }
}
