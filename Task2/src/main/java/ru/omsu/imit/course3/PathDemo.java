package ru.omsu.imit.course3;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;

public class PathDemo {
    public static void main(String[] args) throws IOException {

        Path path1 = Paths.get("D:/1/1.dat");
        Path path2 = FileSystems.getDefault().getPath("D:/1.dat");
        Path path3 = Paths.get("1.dat");
        FileSystem fs = path2.getFileSystem();
        System.out.println(fs);
        System.out.println(path1.isAbsolute());
        System.out.println(path2.isAbsolute());
        System.out.println(path3.isAbsolute());
        System.out.println(path1.getFileName());
        System.out.println(path1.getParent());
        System.out.println(path1.getRoot());
        Path path3Absolute = path3.toAbsolutePath();
        System.out.println("Absolute path = " + path3Absolute);
        System.out.println("Parsing path");
        for(Path p : path3Absolute)
            System.out.println(p);
        Iterator<Path> i = path3Absolute.iterator();
        while(i.hasNext())
            System.out.println(i.next());
        File file3 = path3Absolute.toFile();
        System.out.println(file3);
        Path path4 = Paths.get("1.dat");
        boolean exists = Files.exists(path4, LinkOption.NOFOLLOW_LINKS);
        System.out.println(exists);
        boolean isReadable = Files.isReadable(path4);
        System.out.println(isReadable);
        boolean isWriteable = Files.isWritable(path4);
        System.out.println(isWriteable);
        boolean isExecutable = Files.isExecutable(path4);
        System.out.println(isExecutable);
        Path path5 = Paths.get("Move1.dat");
        try {
            Files.move(path4, path5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
			Files.deleteIfExists(path4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BasicFileAttributes attrs = Files.readAttributes(path1, BasicFileAttributes.class);
        System.out.println(attrs.lastModifiedTime());
        System.out.println(Files.getOwner(path1));
    }
}
