package ru.omsu.imit.course3;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class NIODemo {
    public static Trainee inputByByteBuffer(File file) throws IOException, TraineeException {
        try (FileChannel channel = new FileInputStream(file).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate((int)channel.size());
            channel.read(buffer);
            String s = new String(buffer.array(),"UTF-8");
            String[] array = s.split(" ");
            return new Trainee(array[0], array[1], Integer.parseInt(array[2]));
        }
    }
    public static Trainee inputByMappedByteBuffer(File file) throws IOException, TraineeException {
        try (FileChannel channel = new RandomAccessFile(file, "rw").getChannel()) {
            MappedByteBuffer mappedByteBuffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            byte[] bytes = new byte[(int)channel.size()];
            mappedByteBuffer.get(bytes);
            String s = new String(bytes, "UTF-8");
            String[] array = s.split(" ");
            return new Trainee(array[0], array[1], Integer.parseInt(array[2]));
        }
    }
    public static void outputNumbersByMappedByteBuffer(File file) throws IOException {
        int size = 100;
        try (FileChannel channel = new RandomAccessFile(file, "rw").getChannel()) {
            MappedByteBuffer mappedByteBuffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, size * Integer.BYTES);
            for (int i = 0; i < size; i++) {
                mappedByteBuffer.putInt(i);
            }
        }
    }
    public static List<Integer> inputNumbers(File file) throws IOException {
        long size = file.length()/4;
        List<Integer> list = new ArrayList<>();
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream(file))) {
            while (size > 0) {
                list.add(inputStream.readInt());
                size--;
            }
        }
        return list;
    }
    public static ByteBuffer byteBufferSerialization(Trainee trainee) throws IOException {
        return ByteBuffer.wrap(TraineeDemo.byteArraySerialization(trainee));
    }
    public static Trainee deserialization(ByteBuffer byteBuffer) throws IOException, ClassNotFoundException {
        return TraineeDemo.byteArrayDeserialization(byteBuffer.array());
    }
}
