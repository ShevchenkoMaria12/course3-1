package ru.omsu.imit.course3;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

public class DirectoryWatchDemo {
    private static WatchService watcher;
    private static String watchDir = "D:/1/";
    private static void watchFiles() {
        for (;;) {
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }
            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();
                if (kind == OVERFLOW) {
                    continue;
                }
                WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path path = Paths.get(watchDir).resolve(ev.context());
                if (event.kind().equals(ENTRY_CREATE)) {
                    System.out.println("Created " + (Files.isDirectory(path) ? "directory " : "file ") + path);
                }
                if (event.kind().equals(ENTRY_MODIFY)) {
                    System.out.println("Modified " + (Files.isDirectory(path) ? "directory " : "file ") + path);
                }
                if (event.kind().equals(ENTRY_DELETE)) {
                    System.out.println("Deleted " + path);
                }
                continue;
            }
            boolean valid = key.reset();
            if (!valid) {
                break;
            }
        }
    }
    public static void main(String[] args) {
        try {
            watcher = FileSystems.getDefault().newWatchService();
            FileSystems.getDefault().getPath(watchDir).
                    register(watcher, ENTRY_MODIFY, ENTRY_CREATE, ENTRY_DELETE);
            watchFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
