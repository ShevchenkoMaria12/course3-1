package ru.omsu.imit.course3;

import java.io.Serializable;
import java.util.Objects;

public class Trainee implements Serializable {
    //private static final long serialVersionUID = 803745049486954915L;
    private String firstName, lastName;
    private int grade;
    public Trainee(String firstName, String lastName, int grade) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setGrade(grade);
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() { return lastName; }
    public int getGrade() {
        return grade;
    }
    public void setFirstName(String firstName) throws TraineeException {
        checkFirstName(firstName);
        this.firstName = firstName;
    }
    public void setLastName(String lastName) throws TraineeException {
        checkLastName(lastName);
        this.lastName = lastName;
    }
    public void setGrade(int grade) throws TraineeException {
        checkGrade(grade);
        this.grade = grade;
    }
    private static void checkFirstName(String firstName) throws TraineeException {
        if(firstName == null || firstName.length() == 0)
            throw new TraineeException(TraineeErrorCodes.WRONG_FIRSTNAME, firstName);
    }
    private static void checkLastName(String lastName) throws TraineeException {
        if(lastName == null || lastName.length() == 0 )
            throw new TraineeException(TraineeErrorCodes.WRONG_LASTNAME, lastName);
    }
    private static void checkGrade(int grade) throws TraineeException {
        if(grade < 1 || grade > 5 )
            throw new TraineeException(TraineeErrorCodes.WRONG_GRADE, Integer.toString(grade));
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainee trainee = (Trainee) o;
        return grade == trainee.grade &&
                Objects.equals(firstName, trainee.firstName) &&
                Objects.equals(lastName, trainee.lastName);
    }
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, grade);
    }
}

