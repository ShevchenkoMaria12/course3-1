package ru.omsu.imit.course3;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

class FileVisit implements FileVisitor <Path> {
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file.toString().endsWith(".dat")) {
            String s = file.getParent() + "/" + file.getFileName().toString().replace(".dat", ".bin");
            Files.move(file, Paths.get(s));
        }
        return FileVisitResult.CONTINUE;
    }

}

public class FileVisitorDemo {
    public static void main(String[] args) throws IOException {
        Path start = Paths.get("D:/1");
        FileVisit visitor = new FileVisit();
        Files.walkFileTree(start, visitor );
    }
}
