package ru.omsu.imit.course3;

import java.io.*;

public class TraineeDemo {
    public static void outputBySpace(Trainee trainee, File file) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintStream printStream = new PrintStream(file, "UTF-8")) {
            printStream.print(trainee.getFirstName() + " " + trainee.getLastName() + " " + trainee.getGrade());
        }
    }
    public static Trainee inputBySpace(File file) throws IOException, TraineeException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String[] s = reader.readLine().split(" ");
            return new Trainee(s[0], s[1], Integer.parseInt(s[2]));
        }
    }
    public static byte[] byteArraySerialization(Trainee trainee) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(trainee);
        }
        return baos.toByteArray();
    }
    public static Trainee byteArrayDeserialization(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
            return (Trainee)ois.readObject();
        }
    }
}