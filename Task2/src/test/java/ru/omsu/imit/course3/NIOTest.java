package ru.omsu.imit.course3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NIOTest {
    @Test
    public void inputByByteBufferTest() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("test1.txt");
        TraineeDemo.outputBySpace(trainee,file);
        assertEquals(NIODemo.inputByByteBuffer(file),trainee);
    }
    @Test
    public void inputByMappedByteBufferTest() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        File file = new File("test2.txt");
        TraineeDemo.outputBySpace(trainee,file);
        assertEquals(NIODemo.inputByMappedByteBuffer(file),trainee);
    }
    @Test
    public void outputInputNumbersTest() throws IOException {
        File file = new File("test3.txt");
        NIODemo.outputNumbersByMappedByteBuffer(file);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++){
            list.add(i);
        }
        assertEquals(NIODemo.inputNumbers(file), list);
    }
    @Test
    public void nioTest2() throws IOException, TraineeException, ClassNotFoundException {
        Trainee trainee = new Trainee("Peter", "Petrov", 1);
        assertEquals(NIODemo.deserialization(NIODemo.byteBufferSerialization(trainee)), trainee);
    }
}
