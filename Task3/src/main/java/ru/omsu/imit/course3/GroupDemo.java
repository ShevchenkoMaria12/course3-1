package ru.omsu.imit.course3;

import java.util.Arrays;
import java.util.Comparator;

public class GroupDemo {
    public static void sortByGrade(Group group) throws MyException {
        Arrays.sort(group.getTrainees(), Comparator.comparing(Trainee::getGrade));
    }
    public static void sortByName(Group group) throws MyException {
        Arrays.sort(group.getTrainees());
    }
    public static Trainee searchByName(Group group, String name) throws MyException {
        for(Trainee t : group.getTrainees()) {
            if(t.getFirstName().equals(name)) {
                return t;
            }
        }
        return null;
    }
}
