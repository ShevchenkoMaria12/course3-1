package ru.omsu.imit.course3;

public enum ErrorCodes {
    WRONG_TRAINEE_FIRSTNAME("Wrong first name %s"),
    WRONG_TRAINEE_LASTNAME("Wrong last name %s"),
    WRONG_TRAINEE_GRADE("Wrong grade name %s"),
    WRONG_INSTITUTE_NAME("Wrong name %s"),
    WRONG_INSTITUTE_CITY("Wrong city %s"),
    WRONG_GROUP_NAME("Wrong name %s"),
    WRONG_GROUP_TRAINEES("Wrong trainees %s");
    private String message;
    private ErrorCodes(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
