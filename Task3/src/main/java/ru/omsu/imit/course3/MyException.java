package ru.omsu.imit.course3;

public class MyException extends Exception {
    public MyException() {
        super();
    }
    public MyException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
    public MyException(ErrorCodes errorCode, String param) {
        super(String.format(errorCode.getMessage(), param));
    }
}
