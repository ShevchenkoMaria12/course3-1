package ru.omsu.imit.course3;

import java.util.EnumSet;

enum Color {
    RED, GREEN, BLUE
}

public class EnumSetDemo {

    public static void main(String[] args) {
        EnumSet<Color> setNone = EnumSet.noneOf(Color.class);
        EnumSet<Color> setAll = EnumSet.allOf(Color.class);
        EnumSet<Color> setRed = EnumSet.of(Color.RED);
        EnumSet<Color> setRedGreen = EnumSet.range(Color.RED, Color.GREEN);
    }
}