package ru.omsu.imit.course3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MatrixDemo {
    public static Set<Integer[]> getNotSameString(Integer[][] matrix) {
        Set<Integer[]> result = new HashSet<>();
        boolean flag;
        if (matrix != null && matrix.length != 0) {
            int index = 0;
            Set[] buf = new Set[matrix.length];
            for (Integer[] str : matrix) {
                flag = true;
                Set<Integer> strSet = new HashSet<>(Arrays.asList(str));
                for (int i = 0; i < index; i++) {
                    if(buf[i].equals(strSet)) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    result.add(str);
                    buf[index] = strSet;
                    index++;
                }
            }
        }
        return result;
    }
    public static void main(String[] args) {
        Integer[][] matrix = {{1, 2, 3}, {1, 2, 3, 1, 2, 3, 1, 2, 2, 2, 3}, {4, 4, 5, 6}, {4, 6, 5}, {4, 6, 5, 789}};
        for (Integer[] str : getNotSameString(matrix)) {
            for (Integer i :str) {
                System.out.print(i+" ");
            }
            System.out.println();
        }}
}
