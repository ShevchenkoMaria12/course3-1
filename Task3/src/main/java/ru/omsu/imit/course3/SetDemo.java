package ru.omsu.imit.course3;


import java.util.*;

public class SetDemo {
    public static boolean containsTrainee(Set<Trainee> set,Trainee trainee) {
        return set.contains(trainee);
    }
    public static void printSet(Set<Trainee> set) {
        System.out.println(set.toString());
    }
}
