package ru.omsu.imit.course3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ArrayLinkedListsDemo {
    public static void main(String[] args) {
        final int size = 100000;
        List<Integer> list1 = new ArrayList<>(size);
        List<Integer> list2 = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            list1.add(i);
            list2.add(i);
        }
        long timeStart1;
        long timeEnd1;
        long timeStart2;
        long timeEnd2;
        Random random = new Random(System.currentTimeMillis());
        timeStart1 = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            int pos = random.nextInt(size);
            int test1 = list1.get(pos);
        }
        timeEnd1 = System.nanoTime();
        System.out.println("ArrayList Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd1 - timeStart1))) ;
        timeStart2 = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            int pos = random.nextInt(size);
            int test2 = list2.get(pos);
        }
        timeEnd2 = System.nanoTime();
        System.out.println("LinkedList Time to microSec "+TimeUnit.NANOSECONDS.toMicros(timeEnd2 - timeStart2));
    }
}
