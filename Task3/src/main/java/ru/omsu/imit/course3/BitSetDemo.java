package ru.omsu.imit.course3;

import java.util.BitSet;

public class BitSetDemo {
    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        boolean bit100 = bitSet.get(100);
        System.out.println(bit100);
        bitSet.set(100);
        bit100 = bitSet.get(100);
        System.out.println(bit100);
        bitSet.set(10, 20);
        bit100 = bitSet.get(10);
        System.out.println(bit100);
        BitSet bitSet1 = new BitSet();
        bitSet1.set(100);
        bitSet.andNot(bitSet1);
        bit100 = bitSet.get(100);
        System.out.println(bit100);
        bitSet.clear(15, 18);

    }
}
