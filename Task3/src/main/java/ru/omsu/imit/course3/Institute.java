package ru.omsu.imit.course3;

import java.util.Objects;

public class Institute {
    private String name;
    private String city;
    public Institute(String name, String city) throws MyException {
        setName(name);
        setCity(city);
    }
    public String getName() {
        return name;
    }
    public String getCity() {
        return city;
    }
    public void setName(String name) throws MyException {
        checkName(name);
        this.name = name;
    }
    public void setCity(String city) throws MyException {
        checkCity(city);
        this.city = city;
    }
    private static void checkName(String name) throws MyException {
        if(name == null || name.length() == 0)
            throw new MyException(ErrorCodes.WRONG_INSTITUTE_NAME, name);
    }
    private static void checkCity(String city) throws MyException {
        if(city == null || city.length() == 0 )
            throw new MyException(ErrorCodes.WRONG_INSTITUTE_CITY, city);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Institute institute = (Institute) o;
        return Objects.equals(name, institute.name) &&
                Objects.equals(city, institute.city);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }
}
