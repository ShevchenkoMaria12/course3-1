package ru.omsu.imit.course3;

import java.util.Arrays;
import java.util.Objects;

public class Group {
    private String name;
    private Trainee[] trainees;
    public Group(String name, Trainee... trainees) throws MyException {
        setName(name);
        setTrainees(trainees);
    }
    public String getName() {
        return name;
    }
    public Trainee[] getTrainees() {
        return trainees;
    }
    public void setName(String name) throws MyException {
        checkName(name);
        this.name = name;
    }
    public void setTrainees(Trainee[] trainees) throws MyException {
        checkTrainees(trainees);
        this.trainees = trainees;
    }
    private static void checkName(String name) throws MyException {
        if(name == null || name.length() == 0)
            throw new MyException(ErrorCodes.WRONG_GROUP_NAME, name);
    }
    private static void checkTrainees(Trainee[] trainees) throws MyException {
        if(trainees == null || trainees.length == 0)
            throw new MyException(ErrorCodes.WRONG_GROUP_TRAINEES, trainees.toString());
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
                Arrays.equals(trainees, group.trainees);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(trainees);
        return result;
    }
}
