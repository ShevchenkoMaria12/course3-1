package ru.omsu.imit.course3;

import java.io.Serializable;
import java.util.Objects;

public class Trainee implements Serializable, Comparable <Trainee> {
    private String firstName;
    private String lastName;
    private int grade;
    public Trainee(String firstName, String lastName, int grade) throws MyException {
        setFirstName(firstName);
        setLastName(lastName);
        setGrade(grade);
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() { return lastName; }
    public int getGrade() {
        return grade;
    }
    public void setFirstName(String firstName) throws MyException {
        checkFirstName(firstName);
        this.firstName = firstName;
    }
    public void setLastName(String lastName) throws MyException {
        checkLastName(lastName);
        this.lastName = lastName;
    }
    public void setGrade(int grade) throws MyException {
        checkGrade(grade);
        this.grade = grade;
    }
    private static void checkFirstName(String firstName) throws MyException {
        if(firstName == null || firstName.length() == 0)
            throw new MyException(ErrorCodes.WRONG_TRAINEE_FIRSTNAME, firstName);
    }
    private static void checkLastName(String lastName) throws MyException {
        if(lastName == null || lastName.length() == 0 )
            throw new MyException(ErrorCodes.WRONG_TRAINEE_LASTNAME, lastName);
    }
    private static void checkGrade(int grade) throws MyException {
        if(grade < 1 || grade > 5 )
            throw new MyException(ErrorCodes.WRONG_TRAINEE_GRADE, Integer.toString(grade));
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainee trainee = (Trainee) o;
        return grade == trainee.grade &&
                Objects.equals(firstName, trainee.firstName) &&
                Objects.equals(lastName, trainee.lastName);
    }
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, grade);
    }
    @Override
    public String toString() {
        return "Trainee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", grade=" + grade +
                '}';
    }
    @Override
    public int compareTo(Trainee o) {
        int res= this.firstName.compareTo(o.firstName);
        if (res==0){
            res= this.lastName.compareTo(o.lastName);
        }
        return res;
    }
}


