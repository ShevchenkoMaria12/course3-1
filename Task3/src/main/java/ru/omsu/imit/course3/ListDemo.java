package ru.omsu.imit.course3;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListDemo {
    public static void reverseList(List<Trainee> list) {
        Collections.reverse(list);
    }
    public static void rotareList(List<Trainee> list, int position) {
        Collections.rotate(list, position);
    }
    public static void shuffleList(List<Trainee> list) {

        Collections.shuffle(list);
    }
    public static Trainee searchMaxGrade(List<Trainee> list) {
        return Collections.max(list, Comparator.comparing(Trainee::getGrade));
    }
    public static void sortByName(List<Trainee> list) {
        Collections.sort(list);
    }
    public static void sortByGrade(List<Trainee> list) {
        list.sort(Comparator.comparing(Trainee::getGrade));
    }
    public static Trainee searchByName(List<Trainee> list, String name) {
        for(Trainee t : list) {
            if(t.getFirstName().equals(name)) {
                return t;
            }
        }
        return null;
    }
}
