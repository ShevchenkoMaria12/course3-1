package ru.omsu.imit.course3;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class QueueDemo {
    public static List<Trainee> getListFromQueue(Queue<Trainee> queue){
        List<Trainee> list = new ArrayList<>();
        while(!queue.isEmpty()){
            list.add(queue.poll());
        }
        return list;
    }
}
