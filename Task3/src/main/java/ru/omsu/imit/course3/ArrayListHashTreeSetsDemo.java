package ru.omsu.imit.course3;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ArrayListHashTreeSetsDemo {
    public static void main(String[] args) {
        final int size = 100000;
        final int numberOfSearches = 10000;
        List<Integer> arraylist = new ArrayList<>();
        Set<Integer> hashset = new HashSet<>();
        Set<Integer> treeset = new TreeSet<>();
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < size; i++) {
            arraylist.add(random.nextInt(size));
            hashset.add(random.nextInt(size));
            treeset.add(random.nextInt(size));
        }
        long timeStart1;
        long timeEnd1;
        long timeStart2;
        long timeEnd2;
        long timeStart3;
        long timeEnd3;
        timeStart1 = System.nanoTime();
        for (int i = 0 ;i < numberOfSearches ;i++){
            int pos = random.nextInt(size + 1);
            arraylist.contains(pos);
        }
        timeEnd1 = System.nanoTime();
        System.out.println("ArrayList Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd1 - timeStart1))) ;
        timeStart2 = System.nanoTime();
        for (int i = 0 ;i<numberOfSearches ;i++){
            int pos = random.nextInt(size + 1);
            hashset.contains(pos);
        }
        timeEnd2 = System.nanoTime();
        System.out.println("HashSet Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd2 - timeStart2))) ;
        timeStart3 = System.nanoTime();
        for (int i = 0 ;i<numberOfSearches ;i++){
            int pos = random.nextInt(size + 1);
            treeset.contains(pos);
        }
        timeEnd3 = System.nanoTime();
        System.out.println("TreeSet Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd3 - timeStart3))) ;
    }
}
