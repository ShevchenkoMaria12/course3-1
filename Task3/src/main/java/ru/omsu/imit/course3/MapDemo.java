package ru.omsu.imit.course3;

import java.util.Map;

public class MapDemo {
    public static Institute getInstitute(Map<Trainee,Institute> map, Trainee trainee){
        return map.get(trainee);
    }
    public static void printTrainees(Map<Trainee,Institute> map){
        System.out.println(map.keySet().toString());
    }
    public static void printTraineeAndInstitute(Map<Trainee,Institute> map){
        for (Map.Entry<Trainee,Institute> m : map.entrySet()) {
            System.out.println(m.getKey().toString()+"     "+m.getValue().toString());
        }
    }
}
