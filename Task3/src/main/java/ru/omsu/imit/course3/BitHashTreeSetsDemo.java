package ru.omsu.imit.course3;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class BitHashTreeSetsDemo {
    public static void main(String[] args) {
        final int size = 1000000;
        BitSet bitSet= new BitSet();
        Set<Integer> hashSet = new HashSet<>();
        Set<Integer> treeSet = new TreeSet<>();
        long timeStart1;
        long timeEnd1;
        long timeStart2;
        long timeEnd2;
        long timeStart3;
        long timeEnd3;
        timeStart1 = System.nanoTime();
        for (int i = 0 ;i<size ;i++){
            bitSet.set(i);
        }
        timeEnd1 = System.nanoTime();
        System.out.println("BitSet Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd1 - timeStart1))) ;
        timeStart2 = System.nanoTime();
        for (int i = 0 ;i<size ;i++){
            hashSet.add(i);
        }
        timeEnd2 = System.nanoTime();
        System.out.println("HashSet Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd2 - timeStart2))) ;
        timeStart3 = System.nanoTime();
        for (int i = 0 ;i<size ;i++){
            treeSet.add(i);
        }
        timeEnd3 = System.nanoTime();
        System.out.println("TreeSet Time to microSec "+ TimeUnit.NANOSECONDS.toMicros((timeEnd3 - timeStart3))) ;
    }
}
