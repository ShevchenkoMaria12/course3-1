package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

public class GroupTest {
    @Test(expected = MyException.class)
    public void nameExceptionTest() throws MyException {
        Trainee[] trainees = {new Trainee("Ivan","Ivanov",5),
                            new Trainee("Ivan","Ivanov",2)};
        Group group = new Group("ABC", trainees);
        group.setName("");
    }
    @Test(expected = MyException.class)
    public void traineesExceptionTest() throws MyException {
        Trainee[] trainees = {new Trainee("Ivan","Ivanov",5),
                new Trainee("Ivan","Ivanov",2)};
        Group group = new Group("ABC", trainees);
        group.setTrainees(null);
    }
    @Test
    public void sortByNameTest() throws MyException {
        Trainee[] trainees = {new Trainee("Ivan","Ivanov",5),
                new Trainee("Alex","Ivanov",2)};
        Group group = new Group("ABC", trainees);
        GroupDemo.sortByName(group);
        Trainee[] trainees1 = {new Trainee("Alex","Ivanov",2),
                new Trainee("Ivan","Ivanov",5)};
        Group group1 = new Group("ABC", trainees1);
        Assert.assertEquals(group, group1);

    }
    @Test
    public void sortByGradeTest() throws MyException {
        Trainee[] trainees = {new Trainee("Ivan","Ivanov",5),
                new Trainee("Alex","Ivanov",2)};
        Group group = new Group("ABC", trainees);
        GroupDemo.sortByGrade(group);
        Trainee[] trainees1 = {new Trainee("Alex","Ivanov",2),
                new Trainee("Ivan","Ivanov",5)};
        Group group1 = new Group("ABC", trainees1);
        Assert.assertEquals(group, group1);

    }
    @Test
    public void searchByNameTest() throws MyException{
        Trainee[] trainees = {new Trainee("Ivan","Ivanov",5),
                new Trainee("Alex","Ivanov",2)};
        Group group = new Group("ABC", trainees);
        Assert.assertEquals(GroupDemo.searchByName(group, "Alex"),
                new Trainee("Alex","Ivanov",2));

    }
}
