package ru.omsu.imit.course3;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListTest {
    @Test
    public void arrayListTest() throws MyException {
        List<Trainee> list = new ArrayList<>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        List<Trainee> list1 = new ArrayList<>(Arrays.asList(
                new Trainee("Alex", "Sidorov", 5),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Ivan", "Ivanov", 1)));
        ListDemo.reverseList(list);
        assertEquals(list, list1);
        ListDemo.rotareList(list, 3);
        assertEquals(list, list1);
        ListDemo.shuffleList(list);
        assertEquals(ListDemo.searchMaxGrade(list), new Trainee("Alex", "Sidorov", 5));
        ListDemo.sortByName(list);
        List<Trainee> list2 = new ArrayList<>(Arrays.asList(
                new Trainee("Alex", "Sidorov", 5),
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4)));
        assertEquals(list, list2);
        ListDemo.sortByGrade(list);
        List<Trainee> list3 = new ArrayList<>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        assertEquals(list, list3);
    }
    @Test
    public void linkedListTest() throws MyException {
        List<Trainee> list = new LinkedList<>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        List<Trainee> list1 = new LinkedList<>(Arrays.asList(
                new Trainee("Alex", "Sidorov", 5),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Ivan", "Ivanov", 1)));
        ListDemo.reverseList(list);
        assertEquals(list, list1);
        ListDemo.rotareList(list, 3);
        assertEquals(list, list1);
        ListDemo.shuffleList(list);
        assertEquals(ListDemo.searchMaxGrade(list), new Trainee("Alex", "Sidorov", 5));
        ListDemo.sortByName(list);
        List<Trainee> list2 = new LinkedList<>(Arrays.asList(
                new Trainee("Alex", "Sidorov", 5),
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4)));
        assertEquals(list, list2);
        ListDemo.sortByGrade(list);
        List<Trainee> list3 = new ArrayList<>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        assertEquals(list, list3);
    }
}
