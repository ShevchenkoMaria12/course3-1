package ru.omsu.imit.course3;

import org.junit.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class MapTest {
    @Test
    public void hashMapGetTest() throws MyException {
        Map<Trainee, Institute> map = new HashMap<>();
        map.put(new Trainee("Ivan", "Ivanov", 5), new Institute("OmSY", "Omsk"));
        map.put(new Trainee("Vasya", "Petrov", 5), new Institute("OmSTY", "Omsk"));
        MapDemo.printTrainees(map);
        assertEquals(MapDemo.getInstitute(map, new Trainee("Ivan", "Ivanov", 5)),
                new Institute("OmSY", "Omsk"));
    }
    @Test
    public void treeMapGetTest() throws MyException {
        Map<Trainee, Institute> map = new TreeMap<>();
        map.put(new Trainee("Ivan", "Ivanov", 5), new Institute("OmSY", "Omsk"));
        map.put(new Trainee("Vasya", "Petrov", 5), new Institute("OmSTY", "Omsk"));
        MapDemo.printTrainees(map);
        MapDemo.printTraineeAndInstitute(map);
        assertEquals(MapDemo.getInstitute(map, new Trainee("Ivan", "Ivanov", 5)),
                new Institute("OmSY", "Omsk"));
    }
    @Test
    public void treeMapGetWithOtherSurNameTest() throws MyException {
        Map<Trainee, Institute> map = new TreeMap<>(Comparator.comparing(Trainee::getFirstName));
        map.put(new Trainee("Ivan", "Ivanov", 5), new Institute("OmSY", "Omsk"));
        map.put(new Trainee("Vasya", "Petrov", 5), new Institute("OmSTY", "Omsk"));
        MapDemo.printTrainees(map);
        MapDemo.printTraineeAndInstitute(map);
        assertEquals(MapDemo.getInstitute(map, new Trainee("Ivan", "Sidorov", 5)),
                new Institute("OmSY", "Omsk"));
    }
}
