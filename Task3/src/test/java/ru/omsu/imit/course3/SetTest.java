package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class SetTest {
    @Test
    public void containsHashSetTest() throws MyException {
        Set<Trainee> set = new HashSet<Trainee>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        SetDemo.printSet(set);
        Assert.assertTrue(SetDemo.containsTrainee(set, new Trainee("Misha", "Petrov", 4)));
    }
    @Test
    public void containsTreeSetTest() throws MyException {
        Set<Trainee> set = new TreeSet<Trainee>(Arrays.asList(
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5)));
        SetDemo.printSet(set);
        Assert.assertTrue(SetDemo.containsTrainee(set, new Trainee("Misha", "Petrov", 4)));
    }
    @Test
    public void containsTraineeWithOtherSurNameTest() throws MyException {
        Set<Trainee> set= new TreeSet<Trainee>(Comparator.comparing(Trainee::getFirstName));
        Collections.addAll(set,
                new Trainee("Ivan", "Ivanov", 1),
                new Trainee("Misha", "Petrov", 4),
                new Trainee("Alex", "Sidorov", 5));
        SetDemo.printSet(set);
        Assert.assertTrue(SetDemo.containsTrainee(set, new Trainee("Ivan", "IIII", 1)));
    }
}
