package ru.omsu.imit.course3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.IOException;

public class TraineeTest {
    @Test
    public void compareToTest() throws MyException, IOException {
        assertEquals(new Trainee("Ivan","Ivanov",5).compareTo(
                new Trainee("Ivan","Ivanov",5)), 0);
    }
    @Test(expected = MyException.class)
    public void firstNameExceptionTest() throws MyException {
        Trainee trainee1 = new Trainee("Ivan","Ivanov",5);
        trainee1.setFirstName(null);
    }
    @Test(expected = MyException.class)
    public void lastNameExceptionTest() throws MyException {
        Trainee trainee1 = new Trainee("Ivan","Ivanov",5);
        trainee1.setLastName("");
    }
}
