package ru.omsu.imit.course3;

import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.junit.Assert.assertEquals;

public class QueueTest {
    @Test
    public void test1() throws MyException {
        Queue<Trainee> queue = new LinkedList<>();
        Collections.addAll(queue, new Trainee("Ivan", "Ivanov", 4),
                new Trainee("Alex", "Ivanov", 5));
        List<Trainee> list = new LinkedList<>();
        Collections.addAll(list, new Trainee("Ivan", "Ivanov", 4),
                new Trainee("Alex", "Ivanov", 5));
        assertEquals(QueueDemo.getListFromQueue(queue), list);
    }
}
