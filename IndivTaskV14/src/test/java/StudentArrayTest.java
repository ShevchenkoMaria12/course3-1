import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StudentArrayTest {
    @Test
    public void testAdd(){
        List<Grade> grades1 = new ArrayList<Grade>();
        grades1.add(Grade.FIVE);
        grades1.add(Grade.FOUR);
        grades1.add(Grade.TWO);
        List<Grade> grades2 = new ArrayList<Grade>();
        grades2.add(Grade.FIVE);
        grades2.add(Grade.FOUR);
        grades2.add(Grade.TWO);
        StudentArray studentArray = new StudentArray();
        Student s1 = new Student("I", "I", "Ivanov", Gender.MALE, true);
        Student s2 = new StudentWithGoodGrades("A", "A", "Ivanova", Gender.FEMALE,
                true, grades1);
        Student s3 = new StudentWithVeryGoodGrades("V", "V", "Ivanov", Gender.MALE,
                false, grades2);
        studentArray.addStudent(s1);
        studentArray.addStudent(s2);
        studentArray.addStudent(s3);
        List<Student> list = new ArrayList<Student>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        assertEquals(studentArray.getStudentArrays(),list);
    }
    @Test
    public void testRemove(){
        List<Grade> grades1 = new ArrayList<Grade>();
        grades1.add(Grade.FIVE);
        grades1.add(Grade.FOUR);
        grades1.add(Grade.TWO);
        List<Grade> grades2 = new ArrayList<Grade>();
        grades2.add(Grade.FIVE);
        grades2.add(Grade.FOUR);
        grades2.add(Grade.TWO);
        StudentArray studentArray = new StudentArray();
        Student s1 = new Student("I", "I", "Ivanov", Gender.MALE, true);
        Student s2 = new StudentWithGoodGrades("A", "A", "Ivanova", Gender.FEMALE,
                true, grades1);
        Student s3 = new StudentWithVeryGoodGrades("V", "V", "Ivanov", Gender.MALE,
                false, grades2);
        studentArray.addStudent(s1);
        studentArray.addStudent(s2);
        studentArray.addStudent(s3);
        studentArray.removeStudent(s1);
        List<Student> list = new ArrayList<Student>();
        list.add(s2);
        list.add(s3);
        assertEquals(studentArray.getStudentArrays(),list);
    }
}
