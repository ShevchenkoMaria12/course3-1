import java.util.ArrayList;
import java.util.List;

public class StudentArray {
    private List<Student> studentArrays;
    public StudentArray(){
        studentArrays = new ArrayList<Student>();
    }
    public void addStudent(Student student){
        studentArrays.add(student);
    }
    public void removeStudent(Student student){
        studentArrays.remove(student);
    }
    public List<Student> getStudentArrays() {
        return studentArrays;
    }
}
