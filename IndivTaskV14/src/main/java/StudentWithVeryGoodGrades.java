import java.util.List;

public class StudentWithVeryGoodGrades extends StudentWithGoodGrades{
    private List<Grade> grade;
    public StudentWithVeryGoodGrades(String firstName, String midName, String lastName,
                                 Gender gender, Boolean location, List<Grade> grade) {
        super(firstName, midName, lastName, gender, location, grade);
        for(Grade g : grade) {
            if (g != Grade.FOUR) {
                this.grade = grade;
            }
        }
    }
}
