import java.util.List;

public class StudentWithGoodGrades extends Student{
    private List<Grade> grade;
    public StudentWithGoodGrades(String firstName, String midName, String lastName,
                                 Gender gender, Boolean location, List<Grade> grade) {
        super(firstName, midName, lastName, gender, location);
        for(Grade g : grade) {
            if (g != Grade.TWO && g != Grade.THREE) {
                this.grade = grade;
            }
        }
    }
}
