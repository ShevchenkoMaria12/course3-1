package ru.omsu.imit.course3.Exercise13;

import java.util.Date;

class ThreadLocalHolder {
    private static ThreadLocal<Formatter> threadLocal = new ThreadLocal<>();
    public static void set(Formatter val) {
        threadLocal.set(val);
    }
    public static Formatter get() {
        return threadLocal.get();
    }
}
public class FormatterThread implements Runnable {
    private Formatter formatter;
    private Date date;
    public FormatterThread(Formatter formatter, Date date) {
        this.formatter = formatter;
        this.date = date;
    }
    public void run() {
        ThreadLocalHolder.set(formatter);
        System.out.println(ThreadLocalHolder.get().format(date));
    }
}
