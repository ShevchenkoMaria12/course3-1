package ru.omsu.imit.course3.Exercise13;

import java.util.Date;

public class FormatterDemo {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        Thread first = new Thread(new FormatterThread(formatter, new Date()), "1");
        Thread second = new Thread(new FormatterThread(formatter, new Date()), "2");
        Thread third = new Thread(new FormatterThread(formatter, new Date()), "3");
        Thread fourth = new Thread(new FormatterThread(formatter, new Date()), "4");
        Thread fifth = new Thread(new FormatterThread(formatter, new Date()), "5");
        first.start();
        second.start();
        third.start();
        fourth.start();
        fifth.start();
        try {
            first.join();
            second.join();
            third.join();
            fourth.join();
            fifth.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
