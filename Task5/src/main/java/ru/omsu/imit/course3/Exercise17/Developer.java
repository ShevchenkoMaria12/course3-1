package ru.omsu.imit.course3.Exercise17;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Developer implements Runnable {
    private BlockingQueue<Task> queue;
    private BlockingQueue<State> states;
    public Developer(BlockingQueue<Task> queue, BlockingQueue<State> states) {
        this.queue = queue;
        this.states = states;
    }
    public void run() {
        System.out.println("Developer Started");
        for (int i = 0; i < 3; i++) {
            Task task = new Task("Task" + i, new ArrayList<>());
            List<Executable> list = task.getStages();
            for (int j = 0; j < 3; j++) {
                list.add(new Stage("Stage" + j));
            }
            queue.add(task);
            states.add(State.TASK_ADDED);
        }
        states.add(State.DEVELOPER_FINISHED);
        System.out.println("Developer finished");
    }
}

