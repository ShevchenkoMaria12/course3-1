package ru.omsu.imit.course3.Exercise5;

public class AddDeleteThread implements Runnable {
    private AddAndDelete addAndDeleteThread;
    Thread thread;
    String s;
    public AddDeleteThread(AddAndDelete addAndDeleteThread, String s) {
        thread = new Thread(this,"Thread");
        this.addAndDeleteThread = addAndDeleteThread;
        this.s = s;
        thread.start();
    }
    public void run() {
        for (int i = 0; i < 10000; i++) {
            if (s.equals("add")) {
                addAndDeleteThread.add();
                System.out.println("add " + i);
            } else if (s.equals("delete")) {
                addAndDeleteThread.delete();
                System.out.println("delete " + i);
            }
        }
    }
}
