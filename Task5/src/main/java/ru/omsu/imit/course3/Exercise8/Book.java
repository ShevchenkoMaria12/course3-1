package ru.omsu.imit.course3.Exercise8;

import java.util.concurrent.Semaphore;

public class Book {
    private int n;
    static Semaphore semReader = new Semaphore(0);
    static Semaphore semWriter = new Semaphore(1);
    public void get() {
        try {
            semReader.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            System.out.println("Got: " + n);
            semWriter.release();
        }
    }
    public void put(int n) {
        try {
            semWriter.acquire();
            this.n = n;
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            System.out.println("Put: " + n);
            semReader.release();
        }
    }
}
