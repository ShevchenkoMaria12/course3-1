package ru.omsu.imit.course3.Exercise17;

public interface Executable {
    void execute() throws InterruptedException;
}
