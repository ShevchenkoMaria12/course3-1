package ru.omsu.imit.course3.Exercise17;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Demo {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Task> queue = new LinkedBlockingQueue<>();
        BlockingQueue<State> states = new LinkedBlockingQueue<>();
        Thread developer1 = new Thread(new Developer(queue, states));
        Thread developer2 = new Thread(new Developer(queue, states));
        Thread executor = new Thread(new Executor(queue, states));
        developer1.start();
        developer2.start();
        executor.start();
        int developerStarted = 2;
        int developerFinished = 0;
        int taskAdded = 0;
        int taskFinished = 0;
        while (true) {
            State event = states.take();
            if (event == State.DEVELOPER_FINISHED) {
                developerFinished++;
                continue;
            }
            if (event == State.TASK_ADDED) {
                taskAdded++;
                continue;
            }
            if (event == State.TASK_ENDED) {
                taskFinished++;
            }
            if (developerFinished == developerStarted && taskAdded == taskFinished){
                queue.add(new Task(null, new ArrayList<>()));
                break;
            }
        }
        try {
            developer1.join();
            developer2.join();
            executor.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
