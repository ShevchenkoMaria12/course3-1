package ru.omsu.imit.course3.Exercise7;

import java.util.concurrent.Semaphore;

public class PingPongDemo {
    public static void main(String[] args) {
        Semaphore semPing = new Semaphore(1);
        Semaphore semPong = new Semaphore(0);
        new Ping(semPing, semPong);
        new Pong(semPing, semPong);
    }
}
