package ru.omsu.imit.course3.Exercise6;

import java.util.List;

public class AddThread implements Runnable {
    List<Integer> list;
    Thread thread;
    AddThread(List<Integer> list) {
        thread = new Thread(this,"addThread");
        this.list = list;
        thread.start();
    }
    public void run() {
        for (int i = 0; i < 10000; i++) {
            int j = (int) (Math.random() * 10000);
            list.add(j);
            System.out.println("add " + i);
        }
        System.out.println("addThread " + " exiting.");
    }
}
