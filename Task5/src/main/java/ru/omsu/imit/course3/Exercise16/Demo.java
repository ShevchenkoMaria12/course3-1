package ru.omsu.imit.course3.Exercise16;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Demo {
    public static void main(String[] args) {
        BlockingQueue<Task> queue = new LinkedBlockingQueue<>();
        Task[] tasks = new Task[]{new Task("1"), new Task("2"), new Task("3")};
        Thread producer1 = new Thread(new Producer(queue, tasks));
        Thread producer2 = new Thread(new Producer(queue, tasks));
        Thread consumer1 = new Thread(new Consumer(queue));
        Thread consumer2 = new Thread(new Consumer(queue));
        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        try {
            producer1.join();
            producer2.join();
            queue.add(new Task(null));
            queue.add(new Task(null));
            consumer1.join();
            consumer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
