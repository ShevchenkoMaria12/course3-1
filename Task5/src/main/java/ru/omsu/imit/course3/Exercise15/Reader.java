package ru.omsu.imit.course3.Exercise15;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

public class Reader implements Runnable {
    private BlockingQueue<Data> queue;
    public Reader(BlockingQueue<Data> queue) {
        this.queue = queue;
    }
    public void run() {
        System.out.println("Reader: Started");
        while (true) {
            try {
                Data data = queue.take();
                if (data.get() == null) {
                    System.out.println("Reader: finished");
                    break;
                }
                System.out.println("Reader retrieved: " + Arrays.toString(data.get()));
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
