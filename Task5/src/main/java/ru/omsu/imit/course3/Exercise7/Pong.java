package ru.omsu.imit.course3.Exercise7;

import java.util.concurrent.Semaphore;

public class Pong implements Runnable {
    private Semaphore semPing;
    private Semaphore semPong;
    public Pong(Semaphore semPing, Semaphore semPong) {
        this.semPing = semPing;
        this.semPong = semPong;
        new Thread(this, "Pong").start();
    }
    public void run() {
        while (true) {
            try {
                semPong.acquire();
                System.out.println("Pong");
            } catch (InterruptedException e) {
                System.out.println("Pong error");
            } finally {
                semPing.release();
            }
        }
    }
}
