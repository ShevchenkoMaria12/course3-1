package ru.omsu.imit.course3.Exercise17;

public enum State {
    DEVELOPER_FINISHED,
    TASK_ADDED,
    TASK_ENDED
}
