package ru.omsu.imit.course3;

public class FirstThread implements Runnable {
    String name;
    Thread t;
    public FirstThread (String name) {
        this.name = name;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Thread "+name + " exiting.");
    }
}
