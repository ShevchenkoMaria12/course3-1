package ru.omsu.imit.course3.Exercise17;

public class Stage implements Executable {
    private String name;
    public Stage(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void execute() {
        System.out.println("!");
    }
}
