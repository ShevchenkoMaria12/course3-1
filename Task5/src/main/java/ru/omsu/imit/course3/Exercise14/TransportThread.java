package ru.omsu.imit.course3.Exercise14;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class TransportThread implements Runnable {
    private Message message;
    private BlockingQueue<String> queue;
    public TransportThread(String body, String subject, String email, BlockingQueue<String> queue) {
        this.message = new Message(null, email, subject, body);
        this.queue = queue;
    }
    public void run() {
        try {
            while (!queue.isEmpty()) {
                String email = queue.take();
                message.setEmailAddress(email);
                Transport.send(message);
            }
        } catch (IOException |InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}
