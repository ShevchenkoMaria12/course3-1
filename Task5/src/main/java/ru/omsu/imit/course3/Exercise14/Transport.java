package ru.omsu.imit.course3.Exercise14;

import java.io.IOException;
import java.io.RandomAccessFile;

public class Transport {
    public static void send(Message message) throws IOException {
        System.out.println("Start");
        try {
            Thread.sleep(1000);
            try (RandomAccessFile raf = new RandomAccessFile("Messages.txt","rw")) {
                raf.seek(raf.length());
                raf.writeUTF(message.toString()+"\n");
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("End");
    }
}
