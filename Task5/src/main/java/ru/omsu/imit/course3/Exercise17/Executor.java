package ru.omsu.imit.course3.Exercise17;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

public class Executor implements Runnable {
    private BlockingQueue<Task> queue;
    private BlockingQueue<State> states;
    public Executor(BlockingQueue<Task> queue, BlockingQueue<State> states) {
        this.queue = queue;
        this.states = states;
    }
    public void run() {
        System.out.println("Executor Started");
        while (true) {
            try {
                Task task = queue.take();
                if (task.getName() == null) {
                    System.out.println("Executor finished");
                    break;
                }
                task.execute();
                if (!task.isEmpty()){
                    queue.add(task);
                    continue;
                }
                states.add(State.TASK_ENDED);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
