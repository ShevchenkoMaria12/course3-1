package ru.omsu.imit.course3.Exercise15;

public class Data {
    private int[] array;
    public Data(int... array) {
        this.array = array;
    }
    public int[] get() {
        return array;
    }
}
