package ru.omsu.imit.course3.Exercise11;

public class Pong implements Runnable {
    private PingPong pingPong;
    public Pong(PingPong pingPong) {
        this.pingPong = pingPong;
    }
    public void run() {
        while (true) {
            try {
                pingPong.getPong();
            } catch (InterruptedException e) {
                System.out.println("Pong error");
            }
        }
    }
}
