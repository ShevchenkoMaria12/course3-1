package ru.omsu.imit.course3.Exercise10;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeleteThread implements Runnable {
    List<Integer> list;
    Lock lock;
    Thread thread;
    DeleteThread(List<Integer> list, Lock lock) {
        thread = new Thread(this,"deleteThread");
        this.list = list;
        this.lock = lock;
        thread.start();
    }
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                int j = (int) (Math.random() * list.size());
                if (j != 0) {
                    list.remove(j);
                    System.out.println("delete " + i);
                }
            } finally {
                lock.unlock();
            }
        }
        System.out.println("deleteThread " + " exiting.");
    }
}
