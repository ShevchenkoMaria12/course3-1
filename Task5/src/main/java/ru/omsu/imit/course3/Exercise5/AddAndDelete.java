package ru.omsu.imit.course3.Exercise5;

import java.util.List;

public class AddAndDelete {
    private List<Integer> list;
    public AddAndDelete(List<Integer> list) {
        this.list = list;
    }
    public synchronized void add() {
        list.add((int) (Math.random() * 10000));
    }
    public synchronized void delete() {
        int j = (int) (Math.random() * list.size());
        if (j != 0) {
            list.remove(j);
        }
    }
}
