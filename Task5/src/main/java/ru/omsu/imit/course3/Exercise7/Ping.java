package ru.omsu.imit.course3.Exercise7;

import java.util.concurrent.Semaphore;

public class Ping implements Runnable {
    private Semaphore semPing;
    private Semaphore semPong;
    public Ping(Semaphore semPing, Semaphore semPong) {
        this.semPing = semPing;
        this.semPong = semPong;
        new Thread(this, "Ping").start();
    }
    public void run() {
        while (true) {
            try {
                semPing.acquire();
                System.out.println("Ping");
            } catch (InterruptedException e) {
                System.out.println("Ping error");
            } finally {
                semPong.release();
            }
        }
    }
}
