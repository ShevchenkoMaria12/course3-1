package ru.omsu.imit.course3.Exercise12;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyConcurrentHashMap<K, V> implements Map<K, V> {
    private final Map<K, V> map;
    private final ReadWriteLock lock;
    public MyConcurrentHashMap() {
        map = new HashMap<>();
        lock = new ReentrantReadWriteLock();
    }
    public void clear() {
        lock.writeLock().lock();
        map.clear();
        lock.writeLock().unlock();
    }
    public boolean containsKey(Object key) {
        lock.readLock().lock();
        boolean flag = map.containsKey(key);
        lock.readLock().unlock();
        return flag;
    }
    public boolean containsValue(Object value) {
        lock.readLock().lock();
        boolean flag = map.containsValue(value);
        lock.readLock().unlock();
        return flag;
    }
    public Set<Entry<K, V>> entrySet() {
        lock.readLock().lock();
        Set<Entry<K, V>> res = map.entrySet();
        lock.writeLock().unlock();
        return res;
    }
    public V get(Object key) {
        lock.readLock().lock();
        V res = map.get(key);
        lock.readLock().unlock();
        return res;
    }
    public boolean isEmpty() {
        lock.readLock().lock();
        boolean flag = map.isEmpty();
        lock.readLock().unlock();
        return flag;
    }
    public Set<K> keySet() {
        lock.readLock().lock();
        Set<K> res = map.keySet();
        lock.readLock().unlock();
        return res;
    }
    public V put(K key, V value) {
        lock.writeLock().lock();
        V elem = map.put(key, value);
        lock.writeLock().unlock();
        return elem;
    }
    public void putAll(Map<? extends K, ? extends V> map) {
        lock.writeLock().lock();
        this.map.putAll(map);
        lock.writeLock().unlock();
    }
    public V remove(Object key) {
        lock.writeLock().lock();
        V elem = map.remove(key);
        lock.writeLock().unlock();
        return elem;
    }
    public int size() {
        lock.readLock().lock();
        int size = map.size();
        lock.readLock().unlock();
        return size;
    }
    public Collection<V> values() {
        lock.readLock().lock();
        Collection<V> res = map.values();
        lock.readLock().unlock();
        return res;
    }
}
