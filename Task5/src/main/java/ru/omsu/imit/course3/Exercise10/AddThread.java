package ru.omsu.imit.course3.Exercise10;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AddThread implements Runnable {
    List<Integer> list;
    Lock lock;
    Thread thread;
    AddThread(List<Integer> list, Lock lock) {
        thread = new Thread(this,"addThread");
        this.list = list;
        this.lock = lock;
        thread.start();
    }
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                int j = (int) (Math.random() * 10000);
                list.add(j);
                System.out.println("add " + i);
            } finally {
                lock.unlock();
            }
        }
        System.out.println("addThread " + " exiting.");
    }
}
