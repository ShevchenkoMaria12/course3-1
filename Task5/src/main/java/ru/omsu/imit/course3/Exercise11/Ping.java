package ru.omsu.imit.course3.Exercise11;

public class Ping implements Runnable {
    private PingPong pingPong;
    public Ping(PingPong pingPong) {
        this.pingPong = pingPong;
    }
    public void run() {
        while (true) {
            try {
                pingPong.getPing();
            }  catch (InterruptedException e) {
                System.out.println("Ping error");
            }
        }
    }
}
