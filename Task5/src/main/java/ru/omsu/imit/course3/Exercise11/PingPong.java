package ru.omsu.imit.course3.Exercise11;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPong {
    private Lock lock = new ReentrantLock();
    private Condition ping = lock.newCondition();
    private Condition pong = lock.newCondition();
    private boolean flag = true;
    public void getPing() throws InterruptedException {
        lock.lock();
        try {
            while (!flag) {
                ping.await();
            }
                System.out.println("Ping");
                pong.signal();
                flag = false;
        } finally {
            lock.unlock();
        }
    }
    public void getPong() throws InterruptedException {
        lock.lock();
        try {
            while (flag) {
                pong.await();
            }
                System.out.println("Pong");
                ping.signal();
                flag = true;
        } finally {
            lock.unlock();
        }
    }
}
