package ru.omsu.imit.course3;

class NewThread implements Runnable {
    String name;
    Thread t;
    NewThread(String name) {
        this.name = name;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted.");
        }
        System.out.println(name + " exiting.");
    }
}
public class NewThreadDemo {
    public static void main(String args[]) {
        NewThread thread = new NewThread("NewThread");
        System.out.println("Thread is alive: " + thread.t.isAlive());
        try {
            System.out.println("Waiting for thread to finish.");
            thread.t.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Thread One is alive: " + thread.t.isAlive());
        System.out.println("Main thread exiting.");
    }
}
