package ru.omsu.imit.course3.Exercise8;

public class Reader implements Runnable {
    private Book b;
    public Reader(Book b) {
        this.b = b;
        new Thread(this, "Reader").start();
    }
    public void run() {
        while (true) {
            b.get();
        }
    }
}
