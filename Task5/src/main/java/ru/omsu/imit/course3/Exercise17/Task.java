package ru.omsu.imit.course3.Exercise17;

import java.util.List;

public class Task implements Executable {
    private String name;
    private List<Executable> stages;
    public Task(String name, List<Executable> stages) {
        this.name = name;
        this.stages = stages;
    }
    public void execute() throws InterruptedException {
        if (stages.size() != 0) {
            stages.remove(0).execute();
        }
        if (stages.isEmpty()) {
            System.out.println(name + " done");
        }
    }
    public List<Executable> getStages() {
        return stages;
    }
    public String getName() {
        return name;
    }
    public boolean isEmpty() {
        return stages.isEmpty();
    }
}
