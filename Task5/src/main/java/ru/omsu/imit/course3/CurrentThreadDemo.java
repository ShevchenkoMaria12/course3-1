package ru.omsu.imit.course3;

public class CurrentThreadDemo {
    public static void main( String[] args ) {
        Thread thread = Thread.currentThread();
        System.out.println("Current thread: " + thread);
        System.out.println(thread.toString());
        System.out.println("Current thread ID: " + thread.getId());
        System.out.println("Current thread priority: " + thread.getPriority());
        System.out.println("Current thread state: " + thread.getState());
        System.out.println("Thread "+thread.getName()+" is alive: " +thread.isAlive());
        try {
            for (int n = 5; n > 0; n--) {
                System.out.println(n);
                Thread.sleep(3000);
            }
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted");
        }
        System.out.println("Thread "+thread.getName()+" is alive: " +thread.isAlive());
    }
}
