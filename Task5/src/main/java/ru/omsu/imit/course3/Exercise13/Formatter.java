package ru.omsu.imit.course3.Exercise13;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    private SimpleDateFormat simpleDateFormat;
    public Formatter() {
        simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
    }
    public String format(Date date) {
        return simpleDateFormat.format(date);
    }
}
