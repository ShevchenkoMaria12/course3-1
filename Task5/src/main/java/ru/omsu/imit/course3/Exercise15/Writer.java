package ru.omsu.imit.course3.Exercise15;

import java.util.concurrent.BlockingQueue;

public class Writer implements Runnable {
    private BlockingQueue<Data> queue;
    private Data[] data;
    public Writer(BlockingQueue<Data> queue, Data[] data) {
        this.queue = queue;
        this.data = data;
    }
    public void run() {
        System.out.println("Writer: Started");
        for (Data d : data) {
            queue.add(d);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Writer: finished");
    }
}
