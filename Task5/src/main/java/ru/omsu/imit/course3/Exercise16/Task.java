package ru.omsu.imit.course3.Exercise16;

public class Task implements Executable {
    private String name;
    public Task(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void execute() {
        System.out.println("!");
    }
}
