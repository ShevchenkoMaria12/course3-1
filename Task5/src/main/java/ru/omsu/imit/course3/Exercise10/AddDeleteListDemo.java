package ru.omsu.imit.course3.Exercise10;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AddDeleteListDemo {
    public static void main(String args[]) {
        Lock lock = new ReentrantLock();
        List<Integer> list = new ArrayList<>(10000);
        AddThread addThread = new AddThread(list, lock);
        DeleteThread deleteThread = new DeleteThread(list, lock);
        System.out.println("addThread is alive: " + addThread.thread.isAlive());
        System.out.println("deleteThread is alive: " + deleteThread.thread.isAlive());
        try {
            System.out.println("Waiting for threads to finish.");
            addThread.thread.join();
            deleteThread.thread.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("addThread is alive: " + addThread.thread.isAlive());
        System.out.println("deleteThread is alive: " + deleteThread.thread.isAlive());
        System.out.println("Main thread exiting.");
    }
}
