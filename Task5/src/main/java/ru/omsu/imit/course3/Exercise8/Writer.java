package ru.omsu.imit.course3.Exercise8;

public class Writer implements Runnable {
    private Book b;
    public Writer(Book b) {
        this.b = b;
        new Thread(this, "Writer").start();
    }
    public void run() {
        int i = 0;
        while (true) {
            b.put(i);
            i++;
        }
    }
}
