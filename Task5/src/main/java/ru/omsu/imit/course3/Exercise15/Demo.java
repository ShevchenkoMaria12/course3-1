package ru.omsu.imit.course3.Exercise15;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Demo {
    public static void main(String[] args) {
        BlockingQueue<Data> queue = new LinkedBlockingQueue<>();
        Data[] data = new Data[]{new Data(1, 2, 3), new Data(4, 5, 6), new Data(7, 8, 9)};
        Thread writer1 = new Thread(new Writer(queue, data));
        Thread writer2 = new Thread(new Writer(queue, data));
        Thread reader1 = new Thread(new Reader(queue));
        Thread reader2 = new Thread(new Reader(queue));
        writer1.start();
        writer2.start();
        reader1.start();
        reader2.start();
        try {
            writer1.join();
            writer2.join();
            queue.add(new Data (null));
            queue.add(new Data (null));
            reader1.join();
            reader2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
