package ru.omsu.imit.course3;

public class SecondThread implements Runnable {
    String name;
    Thread t;
    public SecondThread (String name) {
        this.name = name;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }
    public void run() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Thread "+name + " exiting.");
    }
}
