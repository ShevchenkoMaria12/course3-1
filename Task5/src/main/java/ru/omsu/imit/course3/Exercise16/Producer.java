package ru.omsu.imit.course3.Exercise16;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private BlockingQueue<Task> queue;
    private Task[] tasks;
    public Producer(BlockingQueue<Task> queue, Task[] tasks) {
        this.queue = queue;
        this.tasks = tasks;
    }
    public void run() {
        System.out.println("Producer: Started");
        for (Task task : tasks) {
            queue.add(task);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Producer: finished");
    }
}
