package ru.omsu.imit.course3.Exercise16;

public interface Executable {
    void execute();
}
