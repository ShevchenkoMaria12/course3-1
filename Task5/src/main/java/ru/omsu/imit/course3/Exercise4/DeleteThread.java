package ru.omsu.imit.course3.Exercise4;

import java.util.ArrayList;
import java.util.List;

public class DeleteThread implements Runnable {
    List<Integer> list;
    Thread thread;
    DeleteThread (List<Integer> list) {
        thread = new Thread(this,"deleteThread");
        this.list = list;
        thread.start();
    }
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                int j = (int) (Math.random() * list.size());
                if (j != 0) {
                    list.remove(j);
                    System.out.println("delete " + i);
                }
            }
        }
        System.out.println("deleteThread " + " exiting.");
    }
}
