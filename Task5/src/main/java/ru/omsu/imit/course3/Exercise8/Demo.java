package ru.omsu.imit.course3.Exercise8;

public class Demo {
    public static void main(String args[]) {
        Book b = new Book();
        new Reader(b);
        new Writer(b);
    }
}
