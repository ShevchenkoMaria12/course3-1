package ru.omsu.imit.course3;

public class FirstSecondThirdThreadDemo {
    public static void main(String args[]) {
        FirstThread thread1 = new FirstThread("First");
        SecondThread thread2= new SecondThread("Second");
        ThirdThread thread3 = new ThirdThread("Third");
        System.out.println("Thread First is alive: " + thread1.t.isAlive());
        System.out.println("Thread Second is alive: " + thread2.t.isAlive());
        System.out.println("Thread Third is alive: " + thread3.t.isAlive());
        try {
            System.out.println("Waiting for threads to finish.");
            thread1.t.join();
            thread2.t.join();
            thread3.t.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Thread First is alive: " + thread1.t.isAlive());
        System.out.println("Thread Second is alive: " + thread2.t.isAlive());
        System.out.println("Thread Third is alive: " + thread3.t.isAlive());
        System.out.println("Main thread exiting.");
    }
}
