package ru.omsu.imit.course3.Exercise11;

public class PingPongDemo {
    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        Thread ping = new Thread(new Ping(pingPong));
        Thread pong = new Thread(new Pong(pingPong));
        ping.start();
        pong.start();

    }
}
