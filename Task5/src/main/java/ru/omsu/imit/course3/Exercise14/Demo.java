package ru.omsu.imit.course3.Exercise14;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) throws IOException {
        File file = new File("email.txt");
        BlockingQueue queue = new LinkedBlockingQueue();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            queue.addAll(bufferedReader.lines().collect(Collectors.toList()));
        }
        System.out.println(queue.toString());
        Thread first = new Thread(new TransportThread("body", "subject", "abc@gmail.com", queue));
        Thread second = new Thread(new TransportThread("body", "subject", "abc@gmail.com", queue));
        first.start();
        second.start();
    }
}
