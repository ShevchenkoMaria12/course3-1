package ru.omsu.imit.course3.Exercise16;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BlockingQueue<Task> queue;
    public Consumer(BlockingQueue<Task> queue) {
        this.queue = queue;
    }
    public void run() {
        System.out.println("Consumer: Started");
        while (true) {
            try {
                Task task = queue.take();
                if (task.getName() == null) {
                    System.out.println("Consumer: finished");
                    break;
                }
                task.execute();
                queue.poll();
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
